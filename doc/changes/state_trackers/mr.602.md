OpenXR: Make sure to restore old EGL display/context/drawables when creating a
client EGL compositor.
